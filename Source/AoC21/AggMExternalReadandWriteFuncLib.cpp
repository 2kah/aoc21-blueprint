// Fill out your copyright notice in the Description page of Project Settings.


#include "AggMExternalReadandWriteFuncLib.h"

FJsonSerializableArray UAggMExternalReadandWriteFuncLib::LoadFileToString(FString Filename)
{
	FString directory = FPaths::GameSourceDir();
	//FString result;
	TArray<FString> result;
	IPlatformFile& file = FPlatformFileManager::Get().GetPlatformFile();

	if (file.CreateDirectory(*directory))
	{
		FString myFile = directory + "/" + Filename;
		//FFileHelper::LoadFileToString(result, *myFile);
		FFileHelper::LoadFileToStringArray(result, *myFile);
	}
	return result;
}
