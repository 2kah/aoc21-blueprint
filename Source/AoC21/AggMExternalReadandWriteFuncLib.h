// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include <Runtime\Core\Public\Misc\FileHelper.h>
#include "AggMExternalReadandWriteFuncLib.generated.h"

/**
 * 
 */
UCLASS()
class AOC21_API UAggMExternalReadandWriteFuncLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

		UFUNCTION(BlueprintCallable, Category = "File I/O")
		static TArray<FString> LoadFileToString(FString Filename);

		/*UFUNCTION(BlueprintCallable, Category = "File I/O")
		static FString SaveStringToFile(FString Filename, FString Data);*/
};
